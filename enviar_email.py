#!/usr/bin/python3

import collections
import fileinput
import smtplib
import sys
from email.mime.text import MIMEText
from string import Template

# Si invisibleconfig contiene un usuario y contraseña, va a tratar de usar eso
# para mandar el mail, sino, va a usar el envío con token.

from invisibleconfig import username, password, fromaddr
from google_send import SendMessage

def SMTPSend(fromaddr, giver_address, msg):
    server = smtplib.SMTP('smtp.gmail.com:587')
    server.starttls()
    server.login(username,password)
    server.sendmail(fromaddr, giver_address, msg)
    server.quit()

with open(sys.argv[1]) as f:
    content = Template(f.read())

receivers = collections.defaultdict(set)
for line in fileinput.input(sys.argv[2:]):
    g_name, g_address, r_name, r_address = line.strip().split(',')
    receivers[(r_name,r_address)].add((g_name,g_address))

for receiver, givers in receivers.items():
    receiver_name, receiver_address = receiver
    for giver in givers:
        (giver_name, giver_address) = giver
        other_givers = ",\n".join(
            "%s (%s)" % (gn,ga) for (gn,ga) in givers - {giver})

        msg = content.substitute(locals())

        print('enviando mail a', giver_address)
        if username and password:
            SMTPSend(fromaddr, giver_address, msg)
        else:
            SendMessage(fromaddr, giver_address, msg)
