import os
from oauth2client import file as oauthfile
from oauth2client import client, tools
from apiclient import errors, discovery
import httplib2
import base64
import email.parser
import email.policy
import email.header

# El usuario se importa para usar como usrid para enviar
from invisibleconfig import username

SCOPES = 'https://mail.google.com/'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Regalos'

def get_credentials():
    credential_dir = '.credentials'
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir, 'gmail.json')
    store = oauthfile.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        credentials = tools.run_flow(flow, store, tools.argparser.parse_args([]))
        print('Storing credentials to ' + credential_path)
    return credentials

def SendMessage(sender, to, body):
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('gmail', 'v1', http=http)

    p = email.parser.Parser()
    msg = p.parsestr(body)
    msg.set_charset("utf-8")
    content = {'raw': base64.urlsafe_b64encode(msg.as_bytes()).decode()}

    try:
        message = (service.users().messages().send(userId=username, body=content).execute())
        print('Message Id: %s' % message['id'])
        return message
    except errors.HttpError as error:
        print('An error occurred: %s' % error)
        return "Error"
    return "OK"

